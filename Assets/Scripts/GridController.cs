﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridController : MonoBehaviour
{
    public bool only_draw_path;
    
    public LayerMask collideable_mask;  // Layer with collideable objects
    public Vector2 grid_world_size;     // Size of our map
    public float node_diameter;   // Length and width of a node
    Node[,] grid;   // 2-dimentional array of nodes

    float node_radius;  // Radius of a node
    int grid_size_x, grid_size_y;   // How many nodes (tiles) our grid exists out of


    // Ran on initilisation
    void Start()
    {
        // Calculate unset variables
        node_radius = node_diameter / 2;
        grid_size_x = Mathf.RoundToInt( grid_world_size.x / node_diameter );
        grid_size_y = Mathf.RoundToInt( grid_world_size.y / node_diameter );

        CreateGrid();
    }


    public int max_size
    {
        get{ return grid_size_x * grid_size_y; }
    }


    /// <summary>
    /// Creates a grid
    /// </summary>
    void CreateGrid()
    {
        grid = new Node[grid_size_x, grid_size_y];
        // Calculate the bottom left corner of the grid_world_size
        Vector3 world_bottom_left = transform.position
            - new Vector3(1,0,0) * grid_world_size.x / 2    // centre x-as of grid
            - new Vector3(0,0,1) * grid_world_size.y / 2;   // centre y-as of grid


        // For each node
        for (int x = 0; x < grid_size_x; x++)
        {
            for (int y = 0; y < grid_size_y; y++)
            {
                // Calculate position in world
                Vector3 world_point = world_bottom_left
                    + new Vector3(1, 0, 0) * (x * node_diameter + node_radius)
                    + new Vector3(0, 0, 1) * (y * node_diameter + node_radius);

                bool is_collideable = Physics.CheckSphere(world_point, node_radius, collideable_mask);  // Check if there are any colliders overlapping the node
                grid[x, y] = new Node(is_collideable, world_point, x, y);   // Create a new node
            }
        }
    }


    /// <summary>
    /// Get the surrounding nodes of a given node
    /// </summary>
    /// <param name="node">the node to get the neighbours of</param>
    /// <returns>A list of nodes</returns>
    ///
    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();   // List that holds the neighbours of the node

        for( int x = -1; x <= 1; x++ )
        {
            for( int y = -1; y <= 1; y++ )
            {
                // If it's the node given (the current node); skip.
                if (x == 0 && y == 0)
                    continue;

                int check_x = node.grid_x + x;
                int check_y = node.grid_y + y;

                // Check if it's inside of the grid. If it is; add to the list.
                if( check_x >= 0 && check_x < grid_size_x
                    && check_y >= 0 && check_y < grid_size_y )
                {
                    neighbours.Add( grid[check_x, check_y] );
                }
            }
        }

        return neighbours;  // A list of Nodes
    }


    /// <summary>
    /// Returns which node is located on a given position
    /// </summary>
    /// <param name="position">Position of node</param>
    public Node GetNodeFromPosition(Vector3 position)
    {
        float percent_x = (position.x + grid_world_size.x / 2) / grid_world_size.x;
        float percent_y = (position.z + grid_world_size.y / 2) / grid_world_size.y;
        percent_x = Mathf.Clamp01(percent_x);
        percent_y = Mathf.Clamp01(percent_y);

        int x = Mathf.RoundToInt((grid_size_x - 1) * percent_x);
        int y = Mathf.RoundToInt((grid_size_y - 1) * percent_y);

        return grid[x, y];
    }


    public List<Node> path;


    // Visualise layer mask
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube( transform.position, new Vector3(grid_world_size.x, 1, grid_world_size.y) );    // Visualise grid

        if(only_draw_path) {
            if( path != null) {
                foreach( Node node in path)
                {
                    Gizmos.color = Color.magenta;
                    Gizmos.DrawCube( node.position, Vector3.one * (node_diameter - .1f) );
                }
            }
        }
        else
        {
            if (grid != null)
            {
                foreach (Node node in grid) // Visualise each node
                {
                    Gizmos.color = (node.is_collideable) ? Color.red : Color.white; // Set colour based on if it's collideable

                    if (path != null)
                        if ( path.Contains(node) )
                            Gizmos.color = Color.magenta;
                    Gizmos.DrawCube( node.position, Vector3.one * (node_diameter - .1f) );
                }
            }
        }
    }
}
