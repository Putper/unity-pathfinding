﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A node is like a tile on a grid
public class Node : IHeapItem<Node>
{

    public bool is_collideable; // If it's collideable
    public Vector3 position;    // Position in world
    public int grid_x, grid_y;  // Position on grid

    public int score_g; // Distance from start
    public int score_h; // Estimated distance to target

    public Node parent;
    int _heap_index;


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="_is_collideable">If characters can position themselves on this node</param>
    /// <param name="_position">Position of node in the world</param>
    /// <param name="_grid_x">X position of node on grid</param>
    /// <param name="_grid_y">Y position of node on grid</param>
    public Node(bool _is_collideable, Vector3 _position, int _grid_x, int _grid_y)
    {
        is_collideable = _is_collideable;
        position = _position;
        grid_x = _grid_x;
        grid_y = _grid_y;
    }


    // Estimated distance from start position to target position when passing current node
    public int score_f { get { return score_g + score_h; } }


    public int heap_index {
        get { return _heap_index; }
        set { _heap_index = value; }
    }


    public int CompareTo(Node node_to_compare)
    {
        int compare = score_f.CompareTo(node_to_compare.score_f);

        if( compare == 0 )
        {
            compare = score_h.CompareTo( node_to_compare.score_h);
        }
        return -compare;
    }
}
