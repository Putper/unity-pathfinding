﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Heap<T> where T : IHeapItem<T>
{

	T[] items;
	int item_count;


	public Heap(int max_size )
	{
		items = new T[max_size];
	}


	public void Add(T item)
	{
		item.heap_index = item_count;
		items[item_count] = item;

		SortUp(item);
		item_count++;
	}


	public T RemoveFirst()
	{
		T first_item = items[0];
		item_count--;
		items[0] = items[item_count];
		items[0].heap_index = 0;

		SortDown(items[0]);
		return first_item;
	}


	public bool contains(T item)
	{
		return Equals( items[item.heap_index], item );
	}


	public void UpdateItem(T item)
	{
		SortUp(item);
	}


	public int count
	{
		get{ return item_count; }
	}


	void SortDown( T item )
	{
		while( true )
		{
			int child_index_left = item.heap_index * 2 + 1;
			int child_index_right = item.heap_index * 2 + 2;
			int swap_index = 0;

			if( child_index_left < item_count )
			{
				swap_index = child_index_left;

				if( child_index_right < item_count )
				{
					if( items[child_index_left].CompareTo( items[child_index_right] ) < 0 )
					{
						swap_index = child_index_right;
					}
				}

				if( item.CompareTo(items[swap_index]) < 0 )
				{
					Swap( item, items[swap_index] );
				}
				else
				{
					return;
				}
			}
			else
			{
				return;
			}
		}
	}


	void SortUp(T  item)
	{
		int parent_index = (item.heap_index-1) / 2;

		while(true)
		{
			T parent_item = items[parent_index];

			if( item.CompareTo(parent_item) > 0)
			{
				Swap(item, parent_item);
			}
			else
			{
				break;
			}

			parent_index = (item.heap_index-1) / 2;
		}
	}


	void Swap(T item_a, T item_b)
	{
		items[item_a.heap_index] = item_b;
		items[item_b.heap_index] = item_a;

		int item_a_index = item_a.heap_index;
		item_a.heap_index = item_b.heap_index;
		item_b.heap_index = item_a_index;
	}
}



public interface IHeapItem<T> : IComparable<T>
{
	int heap_index {
		get;set;
	}
}