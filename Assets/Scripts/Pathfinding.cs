﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class Pathfinding : MonoBehaviour 
{
    public Transform seeker, target;

    GridController grid;


    // Called when script instance is being loaded
    void Start()
    {
        // Set unset variables
        grid = GetComponent<GridController>();

        //grid = (GridController)FindObjectOfType(typeof(GridController));
    }


    // Called every frame
    void Update()
    {
        if( Input.GetButtonDown("Jump") ){
            FindPathTo(seeker.position, target.position);}
    }


    // Find path to a specific target
    void FindPathTo(Vector3 position_start, Vector3 position_target)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        // Get node from the start and target position
        Node node_start = grid.GetNodeFromPosition(position_start);
        Node node_target = grid.GetNodeFromPosition(position_target);

        // Lists for the nodes
        Heap<Node> set_open = new Heap<Node>(grid.max_size); // Set of nodes to be evaluated
        List<Node> set_closed = new List<Node>();   // Set of nodes already evaluated

        set_open.Add(node_start);   // Add the starting node to the open set

        // Loop
        while( set_open.count > 0 )
        {
            Node node_current = set_open.RemoveFirst();    // Current node

            set_closed.Add(node_current);

            // If current node is the target, then we've found the path and can exit out of the loop.
            if( node_current == node_target )
            {
                stopwatch.Stop();
                print("Pathfinding complete: " + stopwatch.ElapsedMilliseconds + "ms");
                ReconstructPath(node_start, node_target);
                return;
            }

            // For each neighbour node of the current node
            foreach( Node neighbour in grid.GetNeighbours(node_current) )
            {
                // If the neighbour is collideable or in the closed set; skip to the next neighbour
                if (neighbour.is_collideable || set_closed.Contains(neighbour))
                    continue;

                // Movement score to neighbour from the start: the current node's distance from start + the distance between the current node and the neighbour
                int new_score_to_neighbour = node_current.score_g + GetDistance(node_current, neighbour);

                // if new path to neighbour is shorter or the neighbour is not in the open set
                if( new_score_to_neighbour < neighbour.score_g || !set_open.contains(neighbour) )
                {
                    // Set a new f cost for neighbour
                    neighbour.score_g = new_score_to_neighbour;
                    neighbour.score_h = GetDistance(neighbour, node_target);

                    neighbour.parent = node_current; // change the neighbour's parent to the current node.

                    // If neighbour is not in the open set; add it to the open set.
                    if( !set_open.contains(neighbour) )
                        set_open.Add(neighbour);
                        set_open.UpdateItem(neighbour);
                }
            }
        }
    }


    // Retrace to starting node until path
    void ReconstructPath(Node node_start, Node node_end)
    {
        List<Node> total_path = new List<Node>();
        Node node_current = node_end;

        while( node_current != node_start )
        {
            total_path.Add(node_current);
            node_current = node_current.parent;
        }

        total_path.Reverse();

        grid.path = total_path;
    }


    // Get the distance between 2 nodes
    int GetDistance( Node node_a, Node node_b )
    {
        int distance_x = Mathf.Abs(node_a.grid_x - node_b.grid_x);
        int distance_y = Mathf.Abs(node_a.grid_y - node_b.grid_y);

        if( distance_x > distance_y )
            return 14*distance_y + 10*(distance_x - distance_y);
        return 14*distance_x + 10*(distance_y - distance_x);
    }

}
